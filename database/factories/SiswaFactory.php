<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Auth\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Siswa>
 */
class SiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'wali_id' => fake()->randomElement(User::where('akses', 'wali')
                ->pluck('id')->toArray()),
            'wali_status' => 'ok',
            'nama' => fake()->name(),
            'nisn' => fake()->numberBetween(1000, 99990),
            'jurusan' => 'RPL',
            'kelas' => 'XII',
            'angkatan' => fake()->randomElement(['2020', '2021', '2022']),
            'user_id' => 1,
            'biaya_id' => 2
        ];
    }
}
