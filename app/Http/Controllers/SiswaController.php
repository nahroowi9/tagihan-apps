<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSiswaRequest;
use App\Http\Requests\UpdateSiswaRequest;
use App\Models\Biaya;
use Illuminate\Http\Request;
use App\Models\Siswa as Model;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class SiswaController extends Controller
{
    private $viewIndex = 'siswa_index';
    private $viewCreate = 'siswa_form';
    private $viewEdit = 'siswa_form';
    private $viewShow = 'siswa_show';
    private $routePrefix = 'siswa';

    private $accessClass = 'Data Siswa';

    public function index(Request $request)
    {
        $models = Model::with('wali', 'user')->latest();
        if ($request->filled('q')) {
            $models = $models->search($request->q);
        }

        return view('operator.' . $this->viewIndex, [
            'models' => $models->paginate(settings()->get('app_pagination', '50')),
            'routePrefix' => $this->routePrefix,
            'title' => 'Data Siswa'
        ]);
    }

    public function create()
    {
        $data = [
            'listBiaya' => Biaya::has('children')->whereNull('parent_id')->pluck('nama', 'id'),
            'model' => new Model(),
            'method' => 'POST',
            'route' => $this->routePrefix . '.store',
            'button' => 'SIMPAN',
            'title' => 'Tambah ' . $this->accessClass,
            'wali' => User::where('akses', 'wali')->pluck('name', 'id')
        ];
        return view('operator.' . $this->viewCreate, $data);
    }

    public function store(StoreSiswaRequest $request)
    {
        $requestData = $request->validated();

        if ($request->hasFile('foto')) {
            $requestData['foto'] = $request->file('foto')->store('public');
        }


        if ($request->filled('wali_id')) {
            $requestData['wali_status'] = 'ok';
        }

        $siswa = Model::create($requestData);
        flash('Data berhasil disimpan');
        return back();
    }

    public function show($id)
    {
        return view('operator.' . $this->viewShow, [
            'model' => Model::find($id),
            'title' => 'Detail Siswa',
            'access_menu' => $this->accessClass
        ]);
    }

    public function edit($id)
    {
        $data = [
            'listBiaya' => Biaya::has('children')->whereNull('parent_id')->pluck('nama', 'id'),
            'model' => Model::findOrFail($id),
            'method' => 'PUT',
            'route' => [$this->routePrefix . '.update', $id],
            'button' => 'UPDATE',
            'title' => 'Ubah ' . $this->accessClass,
            'wali' => User::where('akses', 'wali')->pluck('name', 'id')
        ];

        return view('operator.' . $this->viewEdit, $data);
    }

    public function update(UpdateSiswaRequest $request, $id)
    {
        $requestData = $request->validated();

        $model = Model::findOrFail($id);

        if ($request->hasFile('foto')) {
            !is_null($model->foto) && Storage::delete($model->foto);
            $requestData['foto'] = $request->file('foto')->store('public');
        }

        $requestData['user_id'] = auth()->user()->id;

        if ($request->filled('wali_id')) {
            $requestData['wali_status'] = 'ok';
        }

        $model->fill($requestData);
        $model->save();

        flash('Data berhasil diupdate');
        return redirect()->route($this->routePrefix . '.index');
    }

    public function destroy($id)
    {
        $siswa = Model::findOrFail($id);
        if ($siswa->tagihan->count() >= 1) {
            flash('Data tidak bisa dihapus karena masih memiliki relasi tagihan')->error();
            return back();
        }

        $siswa->delete();
        flash('Data berhasil dihapus');
        return back();
    }
}
