<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function create()
    {
        return view('operator.setting_form');
    }

    public function store(Request $request)
    {
        $dataSettings = $request->except('_token');
        Settings()->set($dataSettings);
        flash('Data sudah disimpan');
        return back();
    }
}
