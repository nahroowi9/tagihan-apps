<?php

namespace App\Http\Controllers;

use App\Models\Tagihan;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class WaliMuridInvoiceController extends Controller
{
    public function show($id)
    {
        $tagihan = Tagihan::waliSiswa()->findOrFail($id);
        $title = 'Cetak Invoice Tagihan Bulan' . $tagihan->tanggal_tagihan->translatedFormat('F Y');
        if (request('output') == 'pdf') {
            $pdf = Pdf::loadView('invoice', compact('tagihan', 'title'));
            $namaFile = "invoice tagihan " . $tagihan->siswa->nama . ' bulan ' . $tagihan->tanggal_tagihan->translatedFormat('F Y') . '.pdf';
            return $pdf->download($namaFile);
        }
        return view('invoice', compact('tagihan', 'title'));
    }
}
