<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\ModelStatus\HasStatuses;

class Siswa extends Model
{
    use HasFactory;
    use SearchableTrait;
    use HasStatuses;
    protected $guarded = [];
    protected $searchable = [
        'columns' => [
            'nama' => '10',
            'nisn' => '10',
        ],
    ];

    /**
     * Get all of the biaya for the Siswa
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function biaya(): BelongsTo
    {
        return $this->belongsTo(Biaya::class,);
    }

    /**
     * Get the user that owns the Siswa
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function wali(): BelongsTo
    {
        return $this->belongsTo(User::class, 'wali_id')->withDefault([
            'name' => 'Belum ada wali murid'
        ]);
    }

    /**
     * Get all of the comments for the Siswa
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tagihan(): HasMany
    {
        return $this->hasMany(Tagihan::class);
    }

    protected static function booted()
    {
        static::creating(function ($siswa) {
            $siswa->user_id = auth()->user()->id;
        });

        static::created(function ($siswa) {
            $siswa->setStatus('aktif');
        });

        static::updating(function ($siswa) {
            $siswa->user_id = auth()->user()->id;
        });
    }
}
