@extends('layouts.app_sneat_wali')

@section('content')
    <div class="row justify-content-center">
        <div class="card">
        <div class="col-md-12">
                <h5 class="fw-bold py-1 mb-4">DATA TAGIHAN</h5>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped mb-4">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Jurusan</th>
                                    <th>Kelas</th>
                                    <th>Tanggal Tagihan</th>
                                    {{-- <th>Jatuh Tempo</th> --}}
                                    <th>Status Pembayaran</th>
                                    <th style="text-align:center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($models as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->siswa->nama }}</td>
                                        <td>{{ $item->siswa->jurusan }}</td>
                                        <td>{{ $item->siswa->kelas }}</td>
                                        <td>{{ $item->tanggal_tagihan->translatedFormat('F Y') }}</td>
                                        <td>
                                            @if ($item->pembayaran->count() >=1)
                                                <a href="{{ route('wali.pembayaran.show', $item->pembayaran->first()->id) }}" 
                                                    class="btn btn-success btn-sm">
                                                    {{ $item->getStatusTagihanWali() }}
                                                </a>
                                            @else
                                                {{ $item->getStatusTagihanWali() }} 
                                            @endif
                                        </td>
                                        {{-- <td>{{ $item->tanggal_jatuh_tempo->translatedFormat('l d-M-Y') }}</td> --}}
                                        
                                        <td style="text-align:center">
                                            @if ($item->status == 'baru' || $item->status == 'angsur')
                                            <a href="{{ route('wali.tagihan.show', $item->id) }}" class="btn btn-primary btn-sm">Lakukan Pembayaran</a>
                                            @else
                                            <a href="#" class="btn btn-success btn-sm">Pembayaran Sudah Lunas</a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8" style="text-align: center">Data tidak ada</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
