@extends('layouts.app_sneat')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $title }}</div>
                <div class="card-body">
                    {!! Form::model($model, 
                        ['route' => $route, 
                        'method' => $method
                    ]) !!}
                    
                    <div class="form-group mt-1">
                        <lable for="nama">Nama Bank</lable>
                        {!! Form::select('bank_id', $listBank, null, ['class' => 'form-control select2' ]) !!}
                        <span class="text-danger">{{ $errors->first('bank_id') }}</span>
                    </div>
                    
                    <div class="form-group mt-3">
                        <lable for="nama_rekening">Nama Pemilik Rekening</lable>
                        {!! Form::text('nama_rekening', null, ['class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('nama_rekening') }}</span>
                    </div>

                    <div class="form-group mt-3">
                        <lable for="nomor_rekening">Nomor Rekening</lable>
                        {!! Form::text('nomor_rekening', null, ['class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('nomor_rekening') }}</span>
                    </div>

                    {!! Form::submit($button, ['class' => 'btn btn-primary mt-3']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
