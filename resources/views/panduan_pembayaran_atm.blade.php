@extends('layouts.app_sneat_blank')
@section('content')
<style>
    #container-panduan {
        font-size: 18px;
    }
</style>
<div class="container" id="container-panduan">
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h1>PANDUAN PEMBAYARAN MELALUI ATM</h1>
                    <h3>
                        <b>Berikut cazra melakukan</b>
                        <b>Transfer lewat internet banking</b>
                    </h3>
                    <div>
                        <li>Masukkan kartu debit ke mesin <b>ATM</b></li>
                        <li>Pilih bahasa.</li>
                        <li>Masukkan Nomor PIN</li>
                        <li>Pilih menu <b>trasfer</b></li>
                        <li>Pilih tujuan <b>trasfer</b>: antra rekekning atau antar bank.</li>
                        <li>Masukkan kode bank jika memilih <b>trasfer</b>: antar bank.</li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
